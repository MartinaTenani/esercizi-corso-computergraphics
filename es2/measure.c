#include "measure.h"

void computeAndShowFrameRate(void){
    static float lastTime = 0.0f;
    static unsigned int frameCount = 0;
    float sumFPS;

    char windowTitle[100];

    float currentTime = (float)getTick()*0.001f;
    //lastTime = currentTIme.
    if(lastTime == 0){
        lastTime = currentTime;
    }

    //increase frame count
    frameCount++;
    if(currentTime - lastTime > 1.0f){
        //inserisco gli fps nel buffer circolare
        circularBuffer[firstInd] = ((float)frameCount)/(currentTime - lastTime);
        //aggiorno lastTime
        lastTime = currentTime;
        
        firstInd = ((firstInd+1)%NELS);
        if(nEls < NELS){
            nEls++;
        }
        frameCount = 0;

        //sommo gli elementi nel buffer circolare
        sumFPS = sumCircularBuffer();
        snprintf(windowTitle, 100, "FPS = %6.2f",sumFPS/nEls);
        glutSetWindowTitle(windowTitle);
    }
}