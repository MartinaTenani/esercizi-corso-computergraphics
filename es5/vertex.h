#ifndef VERTEX_H_INCLUED
#define VERTEX_H_INCLUDED

#include <cglm/cglm.h>
#include <cglm/types-struct.h>

//Definisce una struttura "Vertex" che conterra\' i parametri di ogni vertice della sfera
//Per farlo usa il tipo predefinito il cglm vec4s.

typedef struct Vertex{
    vec4s coords;
    vec3s normal;
    vec2s texCoords;
}Vertex;

#endif 