#version 430 core

#define HEMISPHERE 0

uniform uint object;
uniform vec4 hemColor;

out vec4 colorsOut;

void main(void){
    if (object == HEMISPHERE) colorsOut = hemColor;
}