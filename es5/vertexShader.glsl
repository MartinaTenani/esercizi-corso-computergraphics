#version 430 core

#define HEMISPHERE 0

layout(location=0) in vec4 hemCoords;

uniform mat4 projMat;
uniform mat4 modelViewMat;
uniform uint object;

vec4 coords;

void main(void){
    if(object == HEMISPHERE) coords = hemCoords;

    gl_Position = projMat * modelViewMat * coords;
}