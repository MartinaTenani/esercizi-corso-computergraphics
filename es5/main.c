/* Esercizio 5
    Sistema Solare
*/

//MISURE DEI PIANETI
/*
    //Venere
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.060, 20.0, 20.0);
    glPushMatrix();

    //Terra
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.063, 20.0, 20.0);
    glPushMatrix();

    //Marte
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.033, 20.0, 20.0);
    glPushMatrix();

    //Giove
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.699, 20.0, 20.0);
    glPushMatrix();

    //Saturno
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.582, 20.0, 20.0);
    glPushMatrix();

    //Urano
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.253, 20.0, 20.0);
    glPushMatrix();

    //Nettuno
    glTranslatef(0.0, 0.0, -1.0);
    glutSolidSphere(0.246, 20.0, 20.0);
    glPushMatrix();
*/
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <math.h>

#include "shader.h"
#include "hemisphere.h"
#include "light.h"

static enum object{HEMISPHERE} VAOID; //VAO nomi
static enum buffer{HEM_VERTICES, HEM_INDICES} VBOID; //VBO

//Global variables
static float Xangle = 0.0, Yangle = 0.0, Zangle = 0.0;

//Global ambient
static const vec4 globAmb = {0.2, 0.2, 0.2, 1.0};

//Proprietà della fonte di luce
static const Light light0={
    (vec4){0.0, 0.0, 0.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){1.0, 1.0, 1.0, 1.0},
    (vec4){0.0, 1.5, 3.0, 0.0}
};

//Hemisphere data - gli array per rappresentare l'emisfera
//array dei vertici
static Vertex hemVertices[(HEM_LONGS+1)*(HEM_LATS+1)];
//array degli indici
static unsigned int hemIndices[HEM_LATS][2*(HEM_LONGS+1)];
static int hemCounts[HEM_LATS];
static void* hemOffsets[HEM_LATS];
static vec4 hemColors = {HEM_COLORS};

//dichiarazione delle matrici modelview e projection con una matrice di identità
mat4 modelViewMat = GLM_MAT4_IDENTITY_INIT;
static mat4 projMat = GLM_MAT4_IDENTITY_INIT;
static mat3 normalMat = GLM_MAT3_IDENTITY_INIT;

//Gli ID per le varie operazioni
static unsigned int
programId,
vertexShaderId,
fragmentShaderId,
modelViewMatLoc,
normalMatLoc,
projMatLoc,
objectLoc,
hemColorLoc,
buffer[2],      //VBO
vao[1];         //VAO

static int window_w = 600;
static int window_h = 600;
static int window_pos_x = 200;
static int window_pos_y= 200;

void init(void), display(void), reshape(int w, int h), generateIcoSphere(void), renderPlanets(void), keyInput(unsigned char key, int x, int y);

int main(int argc, char** argv){

    //GLUT initialization
    glutInit(&argc, argv);

    //set OpenGL version
    glutInitContextVersion(4,3);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

    //Creazione della finestra
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(window_w,window_h);
    glutInitWindowPosition(window_pos_x, window_pos_y);
    glutCreateWindow("Solar System");

    //routine
    glutDisplayFunc(display);
    glutKeyboardFunc(keyInput);
    glutReshapeFunc(reshape);    // funzione per il reshape della finestra

    //GLEW check
    GLenum err = glewInit();
    if(GLEW_OK != err){
        printf("glewInit failed: %s.\n", glewGetErrorString(err));
        exit(1);
    }else{
        printf("glewInit success. \n");
    }

    init();
    glutMainLoop();

    return 0;
}

void init(void){

    GLenum glErr;

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
    
    //Create shader program executable
    vertexShaderId = setShader("vertex", "vertexShader.glsl");
    fragmentShaderId = setShader("fragment", "fragmentShader.glsl");
    programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);
    glUseProgram(programId);

    //Inizializzazione emisfero - riempie gli array che costituiscono ll'emisfero
    fillHemisphere(hemVertices, hemIndices, hemCounts, hemOffsets);

    //Genero i VAO e i VBO
    glGenVertexArrays(1, vao);
    glGenBuffers(2, buffer);

    //associo i dati con il vertex shader
    glBindVertexArray(vao[HEMISPHERE]);
    glBindBuffer(GL_ARRAY_BUFFER, buffer[HEM_VERTICES]); //vertex attributes
    glBufferData(GL_ARRAY_BUFFER, sizeof(hemVertices), hemVertices, GL_STATIC_DRAW); 
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer[HEM_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(hemIndices), hemIndices, GL_STATIC_DRAW);

    //Allocazione coordinate x, y, z, w
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(hemVertices[0]), 0);
    glEnableVertexAttribArray(0);

    //Allocazione coordinate x, y, z della normale
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(hemVertices[0]), (void*)sizeof(hemVertices[0].coords));
    glEnableVertexAttribArray(1);

    //Allocazione coordinate per la texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(hemVertices[0]), (void*)(sizeof(hemVertices[0].coords)+sizeof(hemVertices[0].normal)));
    glEnableVertexAttribArray(2);

    //Richiedo la matrice di proiezione e ne setto il valore
    projMatLoc = glGetUniformLocation(programId, "projMat");
    glm_frustum(-5.0, 5.0, -5.0, 5.0, 5.0, 100.0, projMat); //imposto la viewing frustum
    glUniformMatrix4fv(projMatLoc, 1, GL_FALSE, (GLfloat*) projMat);

    //Richiedo i colori e ne setto il valore
    hemColorLoc = glGetUniformLocation(programId, "hemColor");
    glUniform4fv(hemColorLoc, 1, &hemColors[0]);

    //Richiedo la matrice di modelview
    modelViewMatLoc = glGetUniformLocation(programId, "modelViewMat");
    objectLoc = glGetUniformLocation(programId, "object");

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    //controllo per errori
    if((glErr = glGetError())!=0){
        printf("Error: %d \n", glErr);
        exit(-1);
    }
}

void display(void){

    //clear di frame e depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //calcolo e aggiorno la matrice di Modelview
    glm_mat4_identity(modelViewMat);

    glm_translate(modelViewMat, (vec3){0.0, 0.0,-25.0});
    glm_rotate(modelViewMat, Zangle, (vec3){0.0, 0.0, 1.0});
    glm_rotate(modelViewMat, Yangle, (vec3){0.0, 1.0, 0.0});
    glm_rotate(modelViewMat, Xangle, (vec3){1.0, 0.0, 0.0});
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat*) modelViewMat);

    //Disegno la sfera come unione di due emisfere
    glUniform1ui(objectLoc, HEMISPHERE);    //update del nome
    glBindVertexArray(vao[HEMISPHERE]);
    glMultiDrawElements(GL_TRIANGLE_STRIP, hemCounts, GL_UNSIGNED_INT, (const void **)hemOffsets, HEM_LATS);
    glm_scale(modelViewMat, (vec3){1.0, -1.0, 1.0}); //Disegnamo l'emisfero invertito.
    glUniformMatrix4fv(modelViewMatLoc, 1, GL_FALSE, (GLfloat *)modelViewMat);
    glMultiDrawElements(GL_TRIANGLE_STRIP, hemCounts, GL_UNSIGNED_INT, (const void **)hemOffsets, HEM_LATS);

    //swap per il double buffering
    glutSwapBuffers();
}

//gestione degli input da tastiera
void keyInput(unsigned char key, int x, int y){
    switch(key){
        //ESC
        case 27:
            exit(0);
            break;
        case 'x':
            Xangle += (5.0/360.0)*(2.0*M_PI);
            if(Xangle>(2.0*M_PI)) Xangle -= 2.0*M_PI;
            glutPostRedisplay();
            break;
        case 'X':
            Xangle -= (5.0/360.0)*(2.0*M_PI);
            if(Xangle<0.0) Xangle += 2.0*M_PI;
            glutPostRedisplay();
            break;
        case 'y':
            Yangle += (5.0/360.0)*(2.0*M_PI);
            if(Yangle>(2.0*M_PI)) Yangle -= 2.0*M_PI;
            glutPostRedisplay();
            break;
        case 'Y':
            Yangle -= (5.0/360.0)*(2.0*M_PI);
            if(Yangle<0.0) Yangle += 2.0*M_PI;
            glutPostRedisplay();
            break;
        case 'z':
            Zangle += (5.0/360.0)*(2.0*M_PI);
            if(Zangle>(2.0*M_PI)) Zangle -= 2.0*M_PI;
            glutPostRedisplay();
            break;
        case 'Z':
            Zangle -= (5.0/360.0)*(2.0*M_PI);
            if(Zangle<0.0) Zangle += 2.0*M_PI;
            glutPostRedisplay();
            break;
        default:
            //TO SET OR NOTHING
            break;
    }
}
/*
void renderPlanets(void){
    //proprietà dei materiali (Bronzo)
    static GLfloat matBronzeDiff[4] = {0.8, 0.6, 0.0, 1.0};
    static GLfloat matBronzeSpec[4] = {1.0, 1.0, 0.0, 1.0};
    static GLfloat matBronzeShine[1] = {2.0};

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    //Mercurio
    glTranslatef(0.0, 0.0, -2.0);
    //proprietà del materiale
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, matBronzeDiff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, matBronzeSpec);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, matBronzeShine);
    glColor4fv(matBronzeDiff);
    glutSolidSphere(0.25, 36.0, 36.0);
    glPopMatrix();

}
*/

void reshape(GLint width, GLint height){
   glViewport(0, 0, width, height);
/*
    if (width == 0 || height == 0) return;   
    glMatrixMode(GL_PROJECTION);  
    glLoadIdentity(); 
    gluPerspective(90.0,(GLfloat)width/(GLfloat)height,0.1,100.0);
    glMatrixMode(GL_MODELVIEW);
    glViewport(0,0,width,height);  //Use the whole window for rendering
*/
}