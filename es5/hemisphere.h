#ifndef HEMISPHERE_H_INCLUDED
#define HEMISPHERE_H_INCLUDED

#include "vertex.h"

//Definisce delle macro per la descrizione dell'emisfero.
#define HEM_RADIUS 4.0                     //Raggio dell'emisfero
#define HEM_LONGS 10                       //Numero di tagli longitudinali
#define HEM_LATS 5                         //Numero tagli latitudinali
#define HEM_COLORS  0.0, 1.0, 1.0, 1.0     //Colori dell'emisfero

//Definizione delle funzioni
void fillHemVertexArray(Vertex hemVertices[(HEM_LONGS+1) * (HEM_LATS+1)]);
void fillHemIndices(unsigned int hemIndices[HEM_LATS][2*(HEM_LONGS+1)]);
void fillHemCounts(int hemCounts[HEM_LATS]);
void fillHemOffsets(void* hemOffsets[HEM_LATS]);

void fillHemisphere(Vertex hemVertices[(HEM_LONGS + 1) * (HEM_LATS + 1)], unsigned int hemIndices[HEM_LATS][2*(HEM_LONGS+1)], int hemCounts[HEM_LATS], void* hemOffsets[HEM_LATS]);

#endif
