/*
    1 - Sviluppare un programma OpenGL che, dati 4 punti di controllo, traccia una curva interpolante e una BSplines 
    in termini di una curva di Bezier cubica.

    2 - Partendo dal codice che vi e' stato dato con la lezione 23, scrivere un programma OpenGL che definisce
    una superficie di Bezier, vi associa un materiale, aggiunge una fonte di luce ed esegue il rendering 
    della scena risultante.
*/

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>
#include <math.h>
#include <cglm/cglm.h>
#include <cglm/types-struct.h>

#define  NUM_CONT_POINTS 4

static int window_w = 500;
static int window_h = 500;

// Begin globals.
static int numVal = 0;
static long font = (long)GLUT_BITMAP_8_BY_13; // Font selection.

// Final control points.
static float controlPoints[NUM_CONT_POINTS][3] =
{
    { -15.0, 0.0, 0.0}, { -5.0, 0.0, 0.0}, {5.0, 0.0, 0.0}, {15.0, 0.0, 0.0}
};

// Control point values stored unchanged for use on reset.
static float originalControlPoints[NUM_CONT_POINTS][3] =
{
    { -20.0, 0.0, 0.0}, { -10.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {10.0, 0.0, 0.0}
};
// End globals.

// Initialization routine.
void setup(void)
{
    glClearColor(1.0, 1.0, 1.0, 0.0);
}

// Routine to restore control points to original values.
void restoreControlPoints(void)
{
    int indx, indy;
    for (indx=0; indx<NUM_CONT_POINTS; indx++)
        for (indy=0; indy<3; indy++)
            controlPoints[indx][indy] = originalControlPoints[indx][indy];
}

// Drawing routine.
void drawScene(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.0, 0.0);

/*
    //Separation Line
    glcolor3f(0.0, 0.0, 0.0);
    glBegin(GL_LINES);
        glVertex2i(-window_w, 0);
        glVertex2i(window_w, 0);
    glEnd();
*/

    int indx;

    // Draw the control polygon in light gray.
    glColor3f(0.7, 0.7, 0.7);
    glBegin(GL_LINE_STRIP);
    for (indx = 0; indx < NUM_CONT_POINTS; indx++)
        glVertex3fv(controlPoints[indx]);
    glEnd();

    // Specify and enable the Bezier curve.
    glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, NUM_CONT_POINTS, controlPoints[0]);
    glEnable(GL_MAP1_VERTEX_3);

    // Draw the Bezier curve by defining a sample grid and evaluating on it.
    glColor3f(0.0, 0.0, 0.0);
    glMapGrid1f(100, 0.0, 1.3);
    glEvalMesh1(GL_LINE, 0, 100);

    // Draw the Bezier control points as dots.
    glPointSize(5.0);
    glColor3f(0.0, 1.0, 0.0);
    glBegin(GL_POINTS);
    for (indx = 0; indx < NUM_CONT_POINTS; indx++)
        glVertex3fv(controlPoints[indx]);
    glEnd();

    // Highlight selected control point,
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_POINTS);
    glVertex3fv(controlPoints[numVal]);
    glEnd();

    glutSwapBuffers();
}

// OpenGL window reshape routine.
void resize(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float new_w = w/10;
    float new_h = h/10;
    glOrtho(-new_w, new_w, -new_h, new_h, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);
}

// Keyboard input processing routine.
void keyInput(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27:
        exit(0);
        break;

    case 'r':
        restoreControlPoints();
        glutPostRedisplay();
        break;

    case 'R':
        restoreControlPoints();
        glutPostRedisplay();
        break;

    case ' ':

        if (numVal < NUM_CONT_POINTS-1) numVal++;
        else numVal = 0;

        glutPostRedisplay();
        break;

    default:
        break;
    }
}

// Callback routine for non-ASCII key entry.
void specialKeyInput(int key, int x, int y)
{
    if(key == GLUT_KEY_UP){
        controlPoints[numVal][1] += 1.0;
    }
    if(key == GLUT_KEY_DOWN){
        controlPoints[numVal][1] -= 1.0;
    }
    if(key == GLUT_KEY_LEFT) controlPoints[numVal][0] -= 1.0;
    if(key == GLUT_KEY_RIGHT) controlPoints[numVal][0] += 1.0;

    glutPostRedisplay();
}

// Routine to output interaction instructions to the console window.
void printInteraction(void)
{
    printf("Comandi:\n");
    printf("Spazio per selezionare un punto di controllo da modificare.\n");
    printf("Usa le frecce per modificare la posizione del punto di controllo selezionato.\n");
    printf("r o R per resettare.\n");
}

// Main routine.
int main(int argc, char **argv)
{
    printInteraction();
    glutInit(&argc, argv);

    glutInitContextVersion(4, 3);
    glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(window_w, window_h);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Bezier Curves");
    glutDisplayFunc(drawScene);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyInput);
    glutSpecialFunc(specialKeyInput);

    glewExperimental = GL_TRUE;
    glewInit();

    setup();

    glutMainLoop();
}
