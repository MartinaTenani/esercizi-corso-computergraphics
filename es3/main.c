/*
    Da notare:
    Senza colore--->FPS medi 128

    raddoppiando il numero delle linee il frame rate dimezza

*/

#define _POSIX_C_SOURCE 199309L

#include <GL/glut.h>
#include <unistd.h>
#include <time.h>
#include <stdio.h>

//definisco il numero degli elementi nel buffer circolare.
#define NELS 10
//definisco il numero delle linee
#define NLINES 10000

uint32_t getTick();
inline int modulo(int a, int b);
float sumCircularBuffer(void);
void computeAndShowFrameRate(void);
void display(void);
void init(void);

//dichiaro il buffer circolare che userò per misurare il frame rate.
float circularBuffer[NELS];
int firstInd = 0, nEls = 0;

unsigned int lNumber;

int main(int argc, char **argv){
    glutInit(&argc,argv); //initialize the GLUT library.

    //set display mode
    //GLUT_SINGLE = single buffer window
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(400,400);
    glutInitWindowPosition(100,100);
    glutCreateWindow("OpenGL Window");

    //chiamata alle routine di inizializzazione
    init();
    glutDisplayFunc(display);
    glutMainLoop(); //enters the GLUT event processing loop. 
    return 0;
}

//in init ci vanno quei parametri che vanno inizializzati una volta
void init(void){
    //generazione casuale seed
    srand(time(0));
    //selezione del colore di clear
    glClearColor(0.0, 0.0, 0.0, 0.0);
    //seleziono come metodo di visualizzazione la proiezione ortografica.
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

void display(void){
    int currLineInd;

    //recupero il frame rate corrente
    computeAndShowFrameRate();
    //pulisco il buffer
    glClear(GL_COLOR_BUFFER_BIT);
    for(currLineInd = 0; currLineInd<NLINES; currLineInd++){
        //disegna la riga
        glBegin(GL_LINES);

    for(lNumber = 1; lNumber<=4; lNumber++){
        //seleziono un colore a random
        glColor3f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
        //selezino un punto di partenza a random
        glVertex2f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX);

        glColor3f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
        glVertex2f((float)rand()/RAND_MAX, (float)rand()/RAND_MAX);
    }
    
        glEnd();
    }

    glFinish();
    glutPostRedisplay(); //Mark the normal plane of current window as needing to be redisplayed
}

void computeAndShowFrameRate(void){
    static float lastTime = 0.0f;   //ultimo tempo misurato
    static unsigned int frameCount = 0;     //il frame count registrato
    float sumFPS;

    char windowTitle[100];

    float currentTime = (float)getTick()*0.001f;

    //lastTime = currentTIme.
    if(lastTime == 0){
        lastTime = currentTime;
    }

    //increase frame count
    frameCount++;
    if(currentTime - lastTime > 1.0f){ //lo aggiorna ogni secondo, altrimenti conta solo i frame
        //inserisco gli fps nel buffer circolare
        circularBuffer[firstInd] = ((float)frameCount)/(currentTime - lastTime);
        //aggiorno lastTime
        lastTime = currentTime;
        
        firstInd = ((firstInd+1)%NELS);
        if(nEls < NELS){
            nEls++;
        }
        frameCount = 0; //azzero il frame count

        //sommo gli elementi nel buffer circolare
        sumFPS = sumCircularBuffer();
        snprintf(windowTitle, 100, "FPS = %6.2f",sumFPS/nEls);
        glutSetWindowTitle(windowTitle);
    }
}

int modulo(int a, int b){
    const int result = a%b;
    return result>=0 ? result : result+b;
}

float sumCircularBuffer(void){
    int ind;
    float sum = 0;

    if(nEls>0){
        for(ind=1; ind<=nEls; ind++){
            sum = sum + circularBuffer[modulo(firstInd-ind, NELS)];
        }
    }

    return sum;
}

uint32_t getTick(){
    struct timespec ts;
    unsigned theTick = 0U;
    clock_gettime(CLOCK_REALTIME, &ts);
    theTick = ts.tv_nsec/1000000; 
    theTick += ts.tv_sec * 1000;
    return theTick;
}