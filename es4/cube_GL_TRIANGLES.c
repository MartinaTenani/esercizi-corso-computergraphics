#include <GL/glew.h>        //importante: dichiara PRIMA QUESTA
#include <GL/glut.h>
#include <stdio.h>

#define NFACES 4
#define NVERT 10

// Uncomment ONLY ONE of the following #define
#define TRIANGLES
//#define DRAWARRAYELEMENT
//#define DRAWARRAYS
//#define DRAWELEMENTS_VBO

#if defined (DRAWARRAYELEMENT) || defined (DRAWARRAYS) || defined(DRAWELEMENTS_VBO)
GLfloat vertexArray[NFACES*NVERT*3] = {

    //PRIMA FACCIA
    // first triangle
    0.5, -0.5, 0.5,
    0.2, -0.2, 0.5, 
    0.5, 0.5, 0.5, 
    0.2, 0.2, 0.5,
    -0.5, 0.5, 0.5,
    -0.2, 0.2, 0.5,
    -0.5, -0.5, 0.5,
    -0.2, -0.2, 0.5,
    0.5, -0.5, 0.5,
    0.2, -0.2, 0.5,

    //SECONDA FACCIA
    -0.5, -0.5, 0.5,
    -0.5, -0.2, 0.2,
    -0.5, 0.5, 0.5,
    -0.5, 0.2, 0.2,
    -0.5, 0.5, -0.5,
    -0.5, 0.2, -0.2, 
    -0.5, -0.5, -0.5,
    -0.5, -0.2, -0.2,
    -0.5, -0.5, 0.5,
    -0.5, -0.2, 0.2,

    //TERZA FACCIA
    -0.5, -0.5, -0.5,
    0.2, -0.2, -0.5,
    0.5, -0.5, -0.5,
    0.2, 0.2, -0.5,
    0.5, 0.5, -0.5,
    -0.2, 0.2, -0.5, 
    -0.5, 0.5, -0.5,
    -0.2, -0.2, -0.5,
    -0.5, -0.5, -0.5,
    0.2, -0.2, -0.5,

    //QUARTA FACCIA
    0.5, 0.5, -0.5,
    0.5, -0.2, -0.2,
    0.5, -0.5, -0.5,
    0.5, -0.2, 0.2,
    0.5, -0.5, 0.5,
    0.5, 0.2, 0.2, 
    0.5, 0.5, 0.5,
    0.5, 0.2, -0.2,
    0.5, 0.5, -0.5,
    0.5, -0.2, -0.2,
};

GLfloat colorArray[NFACES*NVERT*3] = {
    //faccia 1
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,
    1.0, 0.0, 0.0,

    //faccia 2
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0,

    //faccia 3
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,
    0.0, 0.0, 1.0,

    //faccia 4
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    0.0, 1.0, 1.0,
    
};
#endif

#ifdef DRAWELEMENTS_VBO
    GLint vertexIndices[NFACES*NVERT];
#endif

//dichiarazioni di funzione
void display(void);
void init (void);

int main(int argc, char**argv){

    //passo gli argomenti a glutInit
    glutInit(&argc, argv);

    //setto la finestra
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);

    glutInitWindowSize(400, 400);
    glutInitWindowPosition (100,100);
    glutCreateWindow("Cube w/ holes");

    // Here we add support for GLEW
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        printf("GLEW init failed: %s\n", glewGetErrorString(err));
        exit(1);
    } else {
        printf("GLEW init success\n");
    };

    init();
    glutDisplayFunc(display);

    glutMainLoop();
    return 0;
}

void init (void){

    GLenum glErr;

#if defined (DRAWARRAYELEMENT) || defined (DRAWARRAYS) 
    glClearColor(0.0, 0.0, 0.0, 0.0);

    //attivo client state
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    //puntatori - dove andare a prendere i dati per il vertex array.
    glVertexPointer(3, GL_FLOAT, 0, vertexArray);
    glColorPointer(3, GL_FLOAT, 0, colorArray);

#endif

#if defined (DRAWELEMENTS_VBO) 

    unsigned int buffers[2], currInd;

    glClearColor(0.0, 0.0, 0.0, 0.0);

    //attivo client state
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glGenBuffers(2, buffers);

    //buffers[0] per vertici e colori
    glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
    //bind e riserva spazio per i due buffer
    glBufferData(GL_ARRAY_BUFFER,sizeof(vertexArray)+sizeof(colorArray), NULL, GL_STATIC_DRAW);
    //copio la prima metà dei dati.
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertexArray),vertexArray);
    //copio la seconda metà dei dati
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertexArray), sizeof(colorArray), colorArray);

    //buffers[1] per gli indici
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
    //inizializzo gli indici
    for(currInd = 0; currInd<NFACES*NVERT; currInd++){
        vertexIndices[currInd]=currInd;
    };
    //copio gli indici
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vertexIndices), vertexIndices, GL_STATIC_DRAW);
    //puntatori - dove andare a prendere i dati per il vertex array.
    glVertexPointer(3, GL_FLOAT, 0, 0);
    glColorPointer(3, GL_FLOAT, 0, (GLvoid*)(sizeof(vertexArray)));
    
#endif

    //inizializzo la matrice
    glMatrixMode(GL_PROJECTION);

    //imposto la modalità di visualizzazione
    glFrustum(-0.2, 0.2, -0.3, 0.1, 0.1, 5.0);

    //controllo se tutto è andato a buon fine
    if((glErr=glGetError())!=0){
        printf("Errore = %d \n", glErr);
        exit(-1);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);
}

void display(void){

    //faccio un clear del color e depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //store
    glPushMatrix();

    //posiziono il cubo nella scena
    glTranslatef(0.0, 0.0, -1.5);
    glTranslatef(0.0, -0.8, 0.0);
    glRotatef(30.0, 0.0, 1.0, 0.0);
    glRotatef(25.0, 1.0, 0.0, 0.0);

    //di seguito ho varie opzioni 

#ifdef TRIANGLES
    glBegin(GL_TRIANGLES);
    //PRIMA FACCIA
    glColor3f(1.0, 0.0, 0.0);
    //primo trapezoide
        //primo triangolo
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(0.5, 0.5, 0.5);
        glVertex3f(0.2, 0.2, 0.5);
        // secondo triangolo
        glVertex3f(0.2, 0.2, 0.5);
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(0.2, -0.2, 0.5);

    // Secondo trapezoide
        // primo triangolo
        glVertex3f(0.5, 0.5, 0.5);
        glVertex3f(-0.5, 0.5, 0.5);
        glVertex3f(0.2, 0.2, 0.5);
        //secondo triangolo
        glVertex3f(-0.5, 0.5, 0.5);
        glVertex3f(0.2, 0.2, 0.5);
        glVertex3f(-0.2, 0.2, 0.5);

    // Terzo trapezoide
        //primo triangolo
        glVertex3f(-0.5, 0.5, 0.5);
        glVertex3f(-0.5, -0.5, 0.5);
        glVertex3f(-0.2, 0.2, 0.5);
        //secondo triangolo 
        glVertex3f(-0.5, -0.5, 0.5);
        glVertex3f(-0.2, -0.2, 0.5);
        glVertex3f(-0.2, 0.2, 0.5);

    // Fourth trapezoid
        //first triangle
        glVertex3f(-0.5, -0.5, 0.5);
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(-0.2, -0.2, 0.5);
        //second triangle
        glVertex3f(0.2, -0.2, 0.5);
        glVertex3f(-0.2, -0.2, 0.5);
        glVertex3f(0.5, -0.5, 0.5);

    //SECONDA FACCIA
    glColor3f(0.0, 1.0, 0.0);
    //primo trapezoide
        //primo triangolo
        glVertex3f(-0.5, -0.5, 0.5);
        glVertex3f(-0.5, 0.5, 0.5);
        glVertex3f(-0.5, -0.2, 0.2);
        // secondo triangolo
        glVertex3f(-0.5, -0.2, 0.2);
        glVertex3f(-0.5, 0.2, 0.2);
        glVertex3f(-0.5, 0.5, 0.5);

    // Secondo trapezoide
        // primo triangolo
        glVertex3f(-0.5, 0.5, 0.5);
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.5, 0.2, 0.2);
        //secondo triangolo
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.5, 0.2, 0.2);
        glVertex3f(-0.5, 0.2, -0.2);

    // Terzo trapezoide
        //primo triangolo
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.5, 0.2, -0.2);
        glVertex3f(-0.5, -0.2, -0.2);
        //secondo triangolo 
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-0.5, -0.2, -0.2);

    // Fourth trapezoid
        //first triangle
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-0.5, -0.2, -0.2);
        glVertex3f(-0.5, -0.2, 0.2);
        //second triangle
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-0.5, -0.2, 0.2);
        glVertex3f(-0.5, -0.5, 0.5);

    //TERZA FACCIA
    glColor3f(0.0, 0.0, 1.0);
    //primo trapezoide
        //primo triangolo
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(-0.2, -0.2, -0.5);
        glVertex3f(-0.5, 0.5, -0.5);
        // secondo triangolo
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.2, 0.2, -0.5);
        glVertex3f(-0.2, -0.2, -0.5);
        
    // Secondo trapezoide
        // primo triangolo
        glVertex3f(-0.5, 0.5, -0.5);
        glVertex3f(-0.2, 0.2, -0.5);
        glVertex3f(0.5, 0.5, -0.5);
        //secondo triangolo
        glVertex3f(-0.2, 0.2, -0.5);
        glVertex3f(0.2, 0.2, -0.5);
        glVertex3f(0.5, 0.5, -0.5);

    // Terzo trapezoide
        //primo triangolo
        glVertex3f(0.5, 0.5, -0.5);
        glVertex3f(0.5, -0.5, -0.5);
        glVertex3f(0.2, 0.2, -0.5);
        //secondo triangolo 
        glVertex3f(0.2, -0.2, -0.5);
        glVertex3f(0.2, 0.2, -0.5);
        glVertex3f(0.5, -0.5, -0.5);

    // Fourth trapezoid
        //first triangle
        glVertex3f(-0.5, -0.5, -0.5);
        glVertex3f(0.5, -0.5, -0.5);
        glVertex3f(0.2, -0.2, -0.5);
        //second triangle
        glVertex3f(-0.2, -0.2, -0.5);
        glVertex3f(0.2, -0.2, -0.5);
        glVertex3f(-0.5, -0.5, -0.5);

    //QUARTA FACCIA
    glColor3f(1.0, 0.0, 1.0);
    //primo trapezoide
        //primo triangolo
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(0.5, 0.5, 0.5);
        glVertex3f(0.5, 0.2, 0.2);
        // secondo triangolo
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(0.5, -0.2, 0.2);
        glVertex3f(0.5, 0.2, 0.2);
        
    // Secondo trapezoide
        // primo triangolo
        glVertex3f(0.5, 0.5, 0.5);
        glVertex3f(0.5, 0.2, 0.2);
        glVertex3f(0.5, 0.2, -0.2);
        //secondo triangolo
        glVertex3f(0.5, 0.5, 0.5);
        glVertex3f(0.5, 0.5, -0.5);
        glVertex3f(0.5, 0.2, -0.2);

    // Terzo trapezoide
        //primo triangolo
        glVertex3f(0.5, 0.5, -0.5);
        glVertex3f(0.5, 0.2, -0.2);
        glVertex3f(0.5, -0.2, -0.2);
        //secondo triangolo 
        glVertex3f(0.5, -0.5, -0.5);
        glVertex3f(0.5, 0.5, -0.5);
        glVertex3f(0.5, -0.2, -0.2);

    // Fourth trapezoid
        //first triangle
        glVertex3f(0.5, -0.5, -0.5);
        glVertex3f(0.5, -0.2, -0.2);
        glVertex3f(0.5, -0.2, 0.2);
        //second triangle
        glVertex3f(0.5, -0.5, -0.5);
        glVertex3f(0.5, -0.5, 0.5);
        glVertex3f(0.5, -0.2, 0.2);
    glEnd();
#endif

#ifdef DRAWARRAYELEMENT
    GLint indFace, indVertex; //numero corrente della faccia, numero corrente del vertice

    // here we draw the triangle strips
    for (indFace=0; indFace<NFACES; indFace++){
        glBegin(GL_TRIANGLE_STRIP);
        for(indVertex = 0; indVertex<NVERT; indVertex++){
            glArrayElement(indFace*NVERT+indVertex);
        };
        glEnd();
    };
#endif

#ifdef DRAWARRAYS
    GLint indFace, indVertex; //numero corrente della faccia, numero corrente del vertice

    for (indFace=0; indFace<NFACES; indFace++){
        glDrawArrays(GL_TRIANGLE_STRIP, indFace*NVERT, 10);
    };
#endif 

#ifdef DRAWELEMENTS_VBO
    GLint indFace, indVertex;
    for(indFace = 0; indFace<NFACES; indFace++){
        glDrawElements(GL_TRIANGLE_STRIP, 10, GL_UNSIGNED_INT, (GLvoid *)(indFace*NVERT*sizeof(GLuint)));
    }
#endif

    //restore model view settings
    glPopMatrix();
    glFinish();

}